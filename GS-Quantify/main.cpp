//
//  main.cpp
//  GS-Quantify
//
//  Created by Ritwik Desai on 25/09/15.
//  Copyright © 2015 Ritwik Desai. All rights reserved.
//

#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <queue>
#include <stack>
#include <utility>
#include <string>
#include <sstream>
#include <cmath>
#include <cfloat>
#define FOPEN(f,c,s) freopen(f,c,s)
using namespace std;

#include "tick-engine.h"

int main(int argc, const char * argv[]) {
    FOPEN("input.in", "r", stdin);
    FOPEN("output.out", "w+", stdout);
    
    int N;
    read_N(N);
    GS_MAP data;
    vector<vector<string>> cases;
    vector<vector<unsigned int>> result;
    read_data(data,cases,N);
    evaluate_data(data,cases,result);
    cout<<"tickfile completed"<<endl;
    for(int i=0;i<result.size();i++){
        for(int j = 0;j<result[i].size();j++){
            cout<<result[i][j]<<" ";
        }
        cout<<endl;
    }
    
}
