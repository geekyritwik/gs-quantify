//
//  tick-engine.h
//  GS-Quantify
//
//  Created by Ritwik Desai on 25/09/15.
//  Copyright © 2015 Ritwik Desai. All rights reserved.
//

#ifndef tick_engine_h
#define tick_engine_h


#endif /* tick_engine_h */
typedef map<unsigned int, map<string,map<string, vector< pair<int,unsigned int> > > > > GS_MAP;
typedef vector<vector<string> > CASEV;
typedef vector<vector<unsigned int> > RESULT;
typedef vector<string> CASE;
typedef map<unsigned int, map<string,map<string, vector<pair<int,unsigned int> > > > > ::iterator GS_MAP_ITR;
typedef  vector<pair<long long , long long>> POINTV;
typedef pair<long long , long long> POINT;
void read_N(int& N){
    string tickfile;
    getline(cin,tickfile);
    stringstream tstream(tickfile);
    string t;
    tstream>>t>>N;
}

void read_data(GS_MAP & data,CASEV & cases,int N){
    int iter = 0;
    while (N--) {
        string line,symbol,variable;
        unsigned int val;
        getline(cin,line);
        stringstream s(line);
        unsigned int tstamp;
        s>>tstamp;
        s>>symbol;
        while(s>>variable>>val){
            data[tstamp][symbol][variable].push_back(make_pair(iter, val));
        }
        
        iter++;
    }
    
    string line;
    while (getline(cin,line)) {
        stringstream l(line);
        string b;
        vector<string> liv;
        while (l>>b) {
            liv.push_back(b);
        }
        cases.push_back(liv);
    }
}

void solveS(CASE& case1,GS_MAP& data,RESULT& result){
    unsigned int start,end;
    string symbol,field;
    stringstream s(case1[1]);
    stringstream e(case1[2]);
    s>>start;
    e>>end;
    symbol = case1[3];
    field = case1[4];
    vector<unsigned int> res;
    unsigned int sum = 0;
    for (GS_MAP_ITR it = data.lower_bound(start); it != data.end() && it->first <=end; it++) {
        if((it->second).find(symbol) != (it->second).end() && (it->second)[symbol].find(field) != (it->second)[symbol].end()){
            //sum += (it->second)[symbol][field];
            for(int i = 0;i<(it->second)[symbol][field].size();i++){
                sum +=(it->second)[symbol][field][i].second;
            }
        }
    }
    res.push_back(sum);
    result.push_back(res);
}
void solveP(CASE& case1,GS_MAP& data,RESULT& result){
    unsigned int start,end;
    string symbol,field1,field2;
    stringstream s(case1[1]);
    stringstream e(case1[2]);
    s>>start;
    e>>end;
    symbol = case1[3];
    field1 = case1[4];
    field2 = case1[5];
    vector<unsigned int> res;
    unsigned int sum = 0;
    for (GS_MAP_ITR it = data.lower_bound(start); it != data.end() && it->first <=end; it++) {
        if((it->second).find(symbol) != (it->second).end() && (it->second)[symbol].find(field1) != (it->second)[symbol].end() &&
           (it->second)[symbol].find(field2) != (it->second)[symbol].end()){
            
                for(int i = 0;i<(it->second)[symbol][field1].size();i++){
                    for(int j = 0;j<(it->second)[symbol][field2].size();j++){
                        if((it->second)[symbol][field1][i].first == (it->second)[symbol][field2][j].first){
                            sum += (it->second)[symbol][field1][i].second * (it->second)[symbol][field2][j].second;
                        }
                    }
                }
        }
    }
    res.push_back(sum);
    result.push_back(res);
}
void solveM(CASE& case1,GS_MAP& data,RESULT& result){
    unsigned int start,end;
    string symbol,field;
    int k;
    stringstream s(case1[1]);
    stringstream e(case1[2]);
    stringstream K(case1[5]);
    s>>start;
    e>>end;
    K>>k;
    symbol = case1[3];
    field = case1[4];
    vector<unsigned int> res;
    vector<unsigned int> tmp;
    for(GS_MAP_ITR it = data.lower_bound(start); it !=data.end() && it->first <=end;it++){
        if((it->second).find(symbol) != (it->second).end() && (it->second)[symbol].find(field) != (it->second)[symbol].end()){
            for(int i = 0;i<(it->second)[symbol][field].size();i++){
                tmp.push_back((it->second)[symbol][field][i].second);
            }
        }
    }
    
    sort(tmp.begin(),tmp.end(),greater<unsigned int>());
    if(k>tmp.size()){
        result.push_back(tmp);
    }else{
        for(int i = 0;i<k;i++){
            res.push_back(tmp[i]);
        }
        result.push_back(res);
    }
    
}

double cal(POINTV& points,int i,int j,int penalty){
    if(i == j) return 0;
    if(j == i+1){
        return 0;
    }else{
        double vv = 0.0;
        if(points[i].first == points[j].first){
            for (int k = i; k<=j; k++) {
                vv += (points[k].first - points[i].first) * (points[k].first - points[i].first);
            }
            return vv;
        }else{
            double num = (double)(points[j].second - points[i].second);
            double dem = (double)(points[j].first - points[i].first);
            double a = num/dem;
            //cout<<"SLOP "<<i<<" "<<j<<" "<<a<<endl;
            
            num = (double)(points[i].second * points[j].first - points[i].first * points[j].second);
            double b = num/dem;
            //cout<<"Intercept "<<i<<" "<<j<<" "<<b<<endl;
            for(int k = i;k<=j;k++){
                double sq = ((double)points[k].second - a * (double)points[k].first - b);
                vv += sq * sq;
            }
            
            return vv ;
        }
    }
}

void solveD(CASE& case1,GS_MAP& data,RESULT& result){
   // cout<<"----"<<endl;
    string symbol,field;
    int penalty;
    stringstream p(case1[3]);
    p>>penalty;
    symbol = case1[1];
    field = case1[2];
    POINTV points;
    for (GS_MAP_ITR it = data.begin(); it != data.end(); it++) {
        if((it->second).find(symbol) != (it->second).end() && (it->second)[symbol].find(field) != (it->second)[symbol].end()){
            for(int i=0;i<(it->second)[symbol][field].size();i++){
                points.push_back(make_pair(it->first, (it->second)[symbol][field][i].second));
            }
        }
    }
    
//    sort(points.begin(),points.end());
//    cout<<"PTS"<<endl;
//    for (int i=0; i<points.size(); i++) {
//        cout<<points[i].first<<" "<<points[i].second<<endl;
//    }
    
    vector<double> DP(points.size(),0.0);
    
    for (int i = 1; i<points.size(); i++) {
        double vv = DBL_MAX;
        for (int j = 0; j<i; j++) {
            double calval = cal(points,j,i,penalty);
            //cout<<j<<" "<<i<<" "<<DP[j]<<"+"<<calval<<endl;
            vv = min(vv,DP[j] + calval+ penalty);
        }
        DP[i] = vv;
    }
    
//    for (int i = 0; i<DP.size(); i++) {
//        cout<<"DP "<<i<<" "<<DP[i]<<endl;
//    }
//    cout<<"----"<<endl;
    int vv = ceil(DP[points.size()-1]);
    
    unsigned int res  = (unsigned int)vv;
    vector<unsigned int> r;
    r.push_back(res);
    result.push_back(r);
}

void solve(string type,CASE& case1 ,GS_MAP& data,RESULT& result){
    if(type.compare("sum") == 0){
        solveS(case1,data,result);
    }else if (type.compare("product") == 0){
        solveP(case1,data,result);
    }else if (type.compare("max") == 0){
        solveM(case1,data,result);
    }else if(type.compare("delta")==0){
        solveD(case1,data,result);
    }
}

void evaluate_data(GS_MAP& data,CASEV& cases,RESULT& result){
    for(int i = 0;i<cases.size();i++){
        string type = cases[i][0];
        solve(type,cases[i],data,result);
    }
}